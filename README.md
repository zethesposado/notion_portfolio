# John Nazareth Esposado

Currently a Flutter Developer Trainee here in the Philippines, a career shifter with 2 years of working experience in the Operation Service Industry.

My focus is on Front-End Development and is using HTML, CSS, JavaScript, and SQL and still studying a new tech stack. I like working on web development and am also interested in doing digital arts.

# Contact Information

LinkedIn: [linkedin.com/in/john-nazareth-esposado](http://linkedin.com/in/john-nazareth-esposado)

Website Portfolio: [https://esposado-flutter-portfolio.herokuapp.com/#/](https://esposado-flutter-portfolio.herokuapp.com/#/)

# Work Experience

## Flutter Developer Trainee

FFUF Manila Inc, from July 2021 to Present

- In training to study how to use Dart, Flutter, Git, and Mobile Application Development.

## Platform Experience New Associate

Accenture, from October 2018 to January 2021

- Test mobile applications such as mobile games, social media apps, productivity apps & more and review their content.
- Provide quality assurance and improve machine classifiers to ensure the safety of the platform for all audiences and cross-functional teams.

# Skills

## Technical Skills

- Front-End Development
- Digital Art
- Graphic Design

## Soft Skills

- Flexible
- Cooperative
- Adaptability

# Education

## BS Information Technology

Pamantasan ng Lungsod ng Valenzuela, 2014-2018